/** @title Graph Visualization Map
 * @author: Artem V L, mailto: lav@lumais.com
 * 			(c) Lumais company: http://www.lumais.com
 * @date: 2013-02
 * @license: Apache v 2.0: http://www.apache.org/licenses/LICENSE-2.0.html with the next extension:
 *		you should provide feedback on the origin page: http://www.lumais.com/gravim
 *		or specify indexed followed link from the page where you embed derivative or origin
 *		of this code to the origin URL of the code: http://www.lumais.com/gravim.
 *		Anyway back link to the code origin is highly appreciated!
 *		Thank you.
 */


// External data processing functions ----------------------------------
/** GraphItem class:
 * vTo - array of nodes id (int) that are linked from this node.
 * 		Note: links are directed
 * vFrom - should be null only for root nodes to mark them otherwise
 * 		should be omitted. Optional
 */    
function GraphItem(vTo, vFrom) {
	if((!(vTo instanceof Array) && vTo !== null)
	|| (!(vFrom instanceof Array) && vFrom != null))
		throw "Invalid input data format for GraphItem: 'vTo' should be Array or null, .from field may be omitted";
	
	this.to = vTo;
	if(vFrom || vFrom === null)
		this.from = vFrom;

	this.toString = function(ext) {
		var opt = this.id != null ? "\n\tid: " + this.id : "";
		if(this.sel != null)
			opt += "\n\tsel: " + this.sel;
		if(this.cx != null)
			opt += "\n\tcx: " + this.cx;  // x center in Navigation View
		if(this.cy != null)
			opt += "\n\tcy: " + this.cy;  // y center in Navigation View
		if(this.x != null)
			opt += "\n\tx: " + this.x;  // x top left of the rect in Session View
		if(this.y != null)
			opt += "\n\ty: " + this.y;  // y top left in Session View
		if(ext) {
			if(this.sub)
				opt += "\n\tsub: " + this.sub;
			if(this.nvNode)
				opt += "\n\tnvNode: " + this.nvNode;
			if(this.nvlTo)
				opt += "\n\tnvlTo: " + this.nvlTo;
			if(this.nvlFrom)
				opt += "\n\tnvlFrom: " + this.nvlFrom;
			if(this.svNode)
				opt += "\n\tsvNode: " + this.svNode;
			if(this.svlTo)
				opt += "\n\tsvlTo: " + this.svlTo;
			if(this.svlFrom)
				opt += "\n\tsvlFrom: " + this.svlFrom;
		}

		return "GraphItem: \n\tto: " + (this.to ? this.to.toString() : null)
		+ (this.from === null || this.from
				? "\n\tfrom: " + (this.from ? this.from.toString() : null)
				: "")
		+ opt + "\n";
	};
}

/** Hierarchy class:
 * root - root level, array of GraphItem
 * depth - number of levels in the hierarchy
 * width - hierarchy width in items = max layer width
 * levsWidth - width of each hierarchy level, array of int 
 */
function Hierarchy(root, depth, width, levsWidth) {
	if(!root || !(levsWidth instanceof Array))
		throw "Invalid input data format for Hierarchy: 'roo' must be not null and 'levsWidth' must be Array";

	this.root = root;  // GraphItem
	this.depth = depth;
	this.width = width;
	this.levsWidth = levsWidth;  // Levels width array [0 .. depth]

	// Optional items:
	var opt = "";
	if(this.seviWidth)
		opt += "\n\tseviWidth: " + this.seviWidth;
	if(this.seviHeight)
		opt += "\n\tseviHeight: " + this.seviHeight;

	this.toString = function() {
		return "Hierarchy: \n\tdepth: " + this.depth
			+ "\n\twidth: " + this.width
			+ "\n\tlevsWidth: " + this.levsWidth
			+ "\n\troot: " + this.root
			+ opt;
	}
}

/** Makes hierarchy from flat list of items
 * 	items - flat array of the graph definition, connectivity matrix,
 * 		constraint: each item is connected with at least one another
 * 			if it is not root.
 * 	returns: Hierarchy instance
 */
function hierarchy(items) {
	if(!(items instanceof Array))
		throw "Invalid input data format: 'items' should be Array";
	// Reset items id (mandatory on hierarchy rebuilding) and
	// Define Root level - array of extended GraphItem
	var root = [];
	items.forEach(function resetId(e, i) {
		if(e.from === null) {
			e.id = i;
			root.push(e);
		}
		else e.id = null;
	});
	if(!root.length) {
		items[0].id = 0;
		root.push(items[0]);
	}
	var width = root.length;  // Hierarchy width, items
	
	// Build Sub levels
	var subLevel = root,
		level;
	var depth = 0;  // Number of levels - hierarchy depth
	var levsWidth = [];
	while(subLevel.length) {
		// Update hierarchy measurements
		if(width < subLevel.length)
			width = subLevel.length;
		levsWidth.push(subLevel.length);
		//console.log("subLevel " + depth + ": " + subLevel);
		level = []
		// Builds unique subnodes of current node as .sub field array
		subLevel.forEach(function buildSubNodes(it) {
			if(!(it.to instanceof Array))
				return;
			it.sub = [];
			// Represent .to ids as .sub items array for not touched ids only
			it.to.forEach(function(v) {
				if(!items[v])  // Some item is not defined, report it to the user
					markErrInputLine(v);
				// Use only first node reference for hierarchy building, i.e. pull nodes up
				if(items[v].id == null) {
					items[v].id = v;  // Mark item by id value definition
					it.sub.push(items[v]);
				}
			});
			if(it.sub.length)
				level = level.concat(it.sub);
		});
		subLevel = level;
		++depth;
	}
	return new Hierarchy(root, depth, width, levsWidth);
}

// Internal data -------------------------------------------------------
/** Navigation View building */
var wsWidth = 800,  // Workspace width and height
	wsRatio = 9/16,  // It is used to set workspace Width depending on Width
	wsHeight = Math.round(wsRatio * wsWidth),
	naviShow = true,  // Show / Hide Navigation View (map)
	naviSzrateK  = 1.4,
	naviSzrate = 1,  // / 1.4,   TODO: Make 3 sizes: small, med, large; rate ~ 1.4
	naviWidth = 320 * naviSzrate,  // Navigation View width and height
	naviHeight = 240 * naviSzrate,
	naviOffs = {x: -1, y: -1},  // Store Navigation View coordinates to handle dragging
	seviOffs = {x: 0, y: 0};  // Session View offset
	
var hier = null,  // Hierarchycal items that is prcessed initial Graph data
	items = null;  // Initial Graph data: flat attay (connetivity matrix) 

// UI items
var inpElem = "textarea#diGraph",
	inpErr = "span#diErr";

/** Mouse events handler
 * Performs both visual elements marking and internal state update
 * e - should be GraphItem instance
 */
function onMouseEvt(e) {
	if(!(e instanceof GraphItem))
		throw "Invalid input for onMouseEvt: 'e' should be instanceof GraphItem";
		
	// Check event type
	var evt = d3.event.type;
	
	if(evt == "mouseover")
		evt = 0;
	else if(evt == "mouseout")
		evt = 1;
	else if(evt == "click")
		evt = 2;
	else return;
	
	if(evt == 2) {
		e.sel = !d3.select(this).classed("selected");
		// Center Session View on selected item
		if(e.sel && d3.event.target.localName == "circle") {
			var x = (e.x + hier.seviWidth / (2 * hier.width)) / hier.seviWidth;
			var y = (e.y  + hier.seviHeight / (2 * hier.depth)) / hier.seviHeight;
			// Move Session View
			seviOffs.x = Math.round(hier.seviWidth * (0.5 -  x) + (wsWidth - hier.seviWidth) / 2);
			seviOffs.y = Math.round(hier.seviHeight * (0.5 - y) + (wsWidth * wsRatio - hier.seviHeight) / 2);
			
			d3.select("svg #sevi")
				.transition()
					.duration(750)
					.attr("transform", "translate(" + [seviOffs.x, seviOffs.y] + ")");
		}
	} else if(d3.select(this).classed("active") == !evt)
		return;  // Item already marked: onmoueover could occur multiple times
	var cls = evt < 2 ? "active" : "selected";
	var val = evt != 2 ? !evt : e.sel;
	var markLnk = function(v) { v.classed(cls, val); };
	
	// Highlight the node
	e.svNode.classed(cls, val);
	// Highlight links
	if(e.svlTo)
		e.svlTo.forEach(markLnk);
	if(e.svlFrom)
		e.svlFrom.forEach(markLnk);

	// Highlight corresponding element in the Navigation View if required
	if(naviUse) {
		// Highlight node
		e.nvNode.classed(cls, val);
		// Highlight links
		if(e.nvlTo)
			e.nvlTo.forEach(markLnk);
		if(e.nvlFrom)
			e.nvlFrom.forEach(markLnk);
	}
}

function dropSelection() {
	d3.selectAll("svg .selected").classed("selected", function(e) { return e.sel = false; });
}

/** Draging
 * Note: additional data {x: .., y: ..} made and joined to this object
 */
var drag = d3.behavior.drag()
		.on("drag", function(d, i) {
			d.x += d3.event.dx;
			d.y += d3.event.dy;
			d3.select(this).attr("transform", function(d, i) {
				return "translate(" + [d.x, d.y] + ")";
			});
		});

/** Build Navigation View
 * hier - instance of Hierarchy
 * items - grapth connectivity matrix, flat array
 */
function buildNavi(hier, items) {
	if(!(hier instanceof Hierarchy && items instanceof Array))
		throw "Invalid data for buildNavi: 'hier' should be instanceof Hierarchy and 'items' should be instanceof Array";
	// Update naviOffs if required
	if(naviOffs.x == -1 && naviOffs.y == -1)
		naviOffs = {x: wsWidth - naviWidth, y: wsHeight - naviHeight};
	d3.select("svg #navi").remove();
	var navi = d3.select("#workSpace svg").append("g")
		.data([naviOffs])
		.attr("id", "navi")
		.attr("transform", "translate(" + [naviOffs.x, naviOffs.y] + ")")
		.call(drag);

	// Prepare background    
	navi.append("rect")
		.attr("id", "navibg")
		.attr("rx", naviWidth / 8)
		.attr("ry", naviHeight / 8)
		.attr("width", naviWidth)
		.attr("height", naviHeight)
		.on("click", dropSelection);

	// Draw Navigation Nodes
	var rNodeMin = 4;  // MIN node radius
	var levDelimMin = 3;  // MIN distance between  centers of the nodes relative to the nodes radius
	var kDelim = Math.max(levDelimMin, Math.min(8 /* Default node delim distance: 8r */, naviHeight / (hier.depth * rNodeMin)));  // Delimiter - vertical distance between nodes relative to node radius
	var rNode = Math.max(rNodeMin, naviHeight / (hier.depth * kDelim));  // Navi node radius
	var diagonal = d3.svg.diagonal()
	var hierLev = hier.root;
	var levNodes = navi.selectAll("circle");  // Root level nodes
	var depth = 0;  // Current hierarchy depth level

	// Scan all hierarchy levels
	while(hierLev.length) {
		var lnd = levNodes.data(hierLev);
		lnd.enter().append("circle")
			.attr("r", rNode)
			.classed("selected", function(d) { return d.sel})
			.on("mouseover", onMouseEvt)
			.on("mouseout", onMouseEvt)
			.on("click", onMouseEvt)
			.attr("cx", function(d, i) { return d.cx = Math.round(naviWidth * (i + 1) / (hierLev.length + 1)); })
			.attr("cy", function(d) {
				d.nvNode = d3.select(this);
				return d.cy = Math.round(rNode * kDelim *(depth + 0.5));
			})
			.attr("title", function(d) { return "id: " + d.id; });
			
		var tmpLev = [];
		hierLev.forEach(function takeNextLevel(gie) {
			if(gie.sub && gie.sub.length)
				tmpLev = tmpLev.concat(gie.sub);
		});
		hierLev = tmpLev;
		++depth;
	}

	// Draw Navigation Links
	var links = navi.selectAll("path.link")
		.data((function toAllLinks(items) {  // Build links array from items array
			var tol = [];
			// Make array of all links, walking through 'to' only to avoid duplications, format: [linkTo, ...]
			items.forEach(function toLinks(it, i) {
				//if(it.from !== null)  // Note: shouldn't be used, because it's just indicator
				if(!it.to)
					return;
				var itLinks = [];
				// Make array of all links from current node, format: linkTo = [id, idTo]
				it.to.forEach(function toLink(id) {
					itLinks.push([i, id]);
				});
				//console.log("itLinks: " + itLinks);
				tol = tol.concat(itLinks);
			});
			return tol;
		})(items));

	links.enter().append("path")
		.attr("class", "link")
		.classed("selected", function(d) { return items[d[0]].sel || items[d[1]].sel; })
		.attr("d", function(d) {
			var lnk = d3.select(this);
			var its = items[d[0]], itt = items[d[1]];

			if(!its.nvlTo)
				its.nvlTo = [];  // d3 Link objects
			its.nvlTo.push(lnk);

			if(!itt.nvlFrom)
				itt.nvlFrom = [];  // d3 Link objects
			itt.nvlFrom.push(lnk);
			
			// Define link edges coordinates
			var s = {x: its.cx, y: its.cy};
			var t = {x: itt.cx, y: itt.cy};
			if(s.y < t.y) {
				s.y += rNode;
				t.y -= rNode;
			} else if(s.y == t.y) {
				s.y += rNode;
				t.x -= rNode;
			} else {
				s.x += rNode;
				t.x -= rNode;
			}

			return diagonal({source: s, target: t});
		});
}

/** Build Session View
 * hier - instance of Hierarchy
 * items - grapth connectivity matrix, flat array
 */
function buildSevi(hier, items) {
	if(!(hier instanceof Hierarchy && items instanceof Array))
		throw "Invalid data for buildSevi: 'hier' should be instanceof Hierarchy and 'items' should be instanceof Array";

	// Build Session View --------------------------------------------------
	d3.select("svg #sevi").remove();
	var sevi = d3.select("#workSpace svg").append("g")
		.data([seviOffs])
		.attr("id", "sevi")
		.attr("transform", "translate(" + [seviOffs.x, seviOffs.y] + ")")
		.call(drag);

	//var hellip = "…";  // Replaces too long items text
	var textLineSize = 21;  // Max size of text line;  font mono, 12px, 3-4 words. Constant
	var textFontSize = 12;  // Current font size
	var nodeWidth = Math.round(156 * textFontSize * textLineSize / (12 * 21));
	var nodeHeight = Math.round(34 * textFontSize / 12);
	var nodeR = Math.round(nodeHeight / 4);
	var nodeDelimRate = 1.618;
	var textLine = "Some item title, long";
	var textSpanLine = "TITLE OULINED HEREg …";
	var textDx = "0.35em";

	var diagonal = d3.svg.diagonal()
	var hierLev = hier.root;
	var levNodes = sevi.selectAll("g");  // Root level nodes
	var depth = 0;  // Current hierarchy depth level

	// Prepare background
	hier.seviWidth = Math.round(hier.width * nodeWidth * nodeDelimRate);
	hier.seviHeight = Math.round(hier.depth * nodeHeight * ( 1 + nodeDelimRate));
	sevi.append("rect")
		.attr("id", "sevibg")
		.attr("width", hier.seviWidth)
		.attr("height", hier.seviHeight);

	// Scan all hierarchy levels
	while(hierLev.length) {
		var dx = Math.round(nodeWidth * nodeDelimRate);
		var dx0 = Math.round(dx * (hier.width - hier.levsWidth[depth]) / 2
			+ (nodeWidth * (nodeDelimRate - 1) / 2));
		var dy0 = Math.round(nodeHeight * nodeDelimRate / 2);
		var dy = Math.round(nodeHeight * (1 + nodeDelimRate) * depth);
		var lnd = levNodes.data(hierLev);
		var lndb = lnd.enter().append("g")
			.attr("id", function(d) { return d.id; })
			.attr("transform", function(d, i) {
				d.svNode = d3.select(this);
				return "translate(" + (d.x = dx0 + dx * i) + ", "  + (d.y = dy0 + dy) + ")"
			}).on("mouseover", onMouseEvt)
			.on("mouseout",  onMouseEvt)
			.on("click", onMouseEvt);
			
		lndb.append("rect")
			.attr({rx: nodeHeight / 4, ry: nodeHeight / 4, width: nodeWidth, height: nodeHeight});
		lndb.append("text")
			.attr("dx", textDx)
			.attr("dy", "1.1em")
			.text(textLine)
				.append("tspan")
				.attr("x", 0)
				.attr("dx", textDx)
				.attr("dy", "1.2em")
				.text(textSpanLine);
		
			
		var tmpLev = [];
		hierLev.forEach(function takeNextLevel(gie) {
			if(gie.sub && gie.sub.length)
				tmpLev = tmpLev.concat(gie.sub);
		});
		hierLev = tmpLev;
		++depth;
	}

	// Draw Session Links
	// TODO: skip drawing links that are out of working space
	var links = sevi.selectAll("path.link")
		.data((function toAllLinks(items) {  // Build links array from items array
			var tol = [];
			// Make array of all links, walking through 'to' only to avoid duplications, format: [linkTo, ...]
			items.forEach(function toLinks(it, i) {
				//if(it.from !== null)  // Note: shouldn't be used, because it's just indicator
				if(!it.to)
					return;
				var itLinks = [];
				// Make array of all links from current node, format: linkTo = [id, idTo]
				it.to.forEach(function toLink(id) {
					itLinks.push([i, id]);
				});
				//console.log("itLinks: " + itLinks);
				tol = tol.concat(itLinks);
			});
			return tol;
		})(items));  // .projection(function(d) { return [d.y, d.x]; }

	links.enter().append("path")
		.attr("class", "link")
		.attr("d", function(d) {
			var lnk = d3.select(this);
			var its = items[d[0]], itt = items[d[1]];

			if(!its.svlTo)
				its.svlTo = [];  // d3 Link objects
			its.svlTo.push(lnk);

			if(!itt.svlFrom)
				itt.svlFrom = [];  // d3 Link objects
			itt.svlFrom.push(lnk);
			
			// Define link edges coordinates
			var dx = Math.round(nodeWidth / 2);
			var dy = Math.round(nodeHeight / 2);
			var s = {x: its.x, y: its.y};
			var t = {x: itt.x, y: itt.y};
			if(s.y < t.y) {
				s.y += nodeHeight;
				s.x += dx;
				t.x += dx;
			} else if(s.y == t.y) {
				s.y += nodeHeight;
				s.x += dx;
				t.y += dy;
			} else {
				s.y += dy;
				s.x += nodeWidth;
				t.y += dy;
			}

			return diagonal({source: s, target: t});
		});
}

/** Marks invalid line in the input data and print the error
 * i - line number where error occurred, int
 * id - inconsistent id, int. Optional
 * msg - error msg. Optional
 */
function markErrInputLine(i, id, msg) {
	var items = [];
	var iMsg;
	d3.select(inpElem)
		.call(function(it) {
			var inp = it.node();
			var its = inp.value.split("\n")
			if(i >= its.length) {
				iMsg = "Invalid line number: not all expected ids are defined";
				return;
			}
			
			if(!its[i].trim().length) {
				if(!msg)
					msg = "Expected item #" + i + " is not defined";
				return;
			}
			
			var pos = 0;
			for(var j = 0; j < i; ++j)
				pos += its[j].length;
			pos += i; // '\n' char
			
			if(inp.setSelectionRange) {  // Standard
				inp.focus();
				inp.setSelectionRange(pos, pos + its[i].length);
			} else if(inp.createTextRange) {  // IE
				var range = inp.createTextRange();
				range.collapse(true);
				range.moveEnd('character', pos + its[i].length);
				range.moveStart('character', pos);
				range.select();
			}			
		});
		
	d3.select(inpErr)
		.call(function note(el) {
			var text = "ERROR ";

			if(!iMsg) {
				text += "Inconsistent id = " + id + "(expected: " + i + ")";
				
				if(msg)
					text += "; " + msg;
			}
			else text += iMsg;
			
			el.node().textContent = text;
		});
}

/** Build all Views (Session & Navigation)
 * items  - flat array of items - grapth connectivity matrix
 * naviOnly - whether to build only Navigation View. Optional
 */
function buildViews(items) {
	// Check initial value and initialize if required
	if(!(items instanceof Array))
		throw "Ivalid data for buildViews: 'items' should be Array, graph connectivity matrix";

	// Build Hierarchy
	/**
	* Optimization TODO: now complexity proportional to the multiplication of
	* coupled graph to the number of it's uncoupled sub graphs. That is not
	* optimal and could be reimplemented.
	*/
	var z = 0, zMax = items.length;  // Anti hang count
	do
		hier = hierarchy(items);
	while(items.some(function idInvalid(it, i) {
			var failed = it.id !== i;
			if(failed) {
				it.from = null;
				if(z == zMax - 1) {
					var msg = "Hierarchy is broken: not all node ids are defined (Graph is not fully connected and not all roots are found)";
					markErrInputLine(i, it.id, msg);
					throw msg;
				}
			}
			return failed;
		}) && ++z < zMax);

	buildSevi(hier, items);
	buildNavi(hier, items);
}

function updateViews() {
	d3.select(inpErr)
		.call(function tidy(el) {
			el.node().textContent = "";
		});
		
	items = parseTextInput(inpElem);
	if(!items.length)
		throw "Invalid input text: parsed data is empty";
	buildViews(items);
}

function wsResize() {
	var w0 = wsWidth,
		h0 = wsHeight;
	// naviMove
	var lws = d3.select("#workSpace");  // Attention: otherwise does not work in Firefox
	var sz = lws.style("width");
	if(sz && sz[sz.length - 1] === 'x')  // ...px
		wsWidth = sz.slice(0, -2);

	if(wsRatio) {
		wsHeight = Math.round(wsWidth * wsRatio);
		lws.select("svg")
			.attr("height", wsHeight);
	} else {
		lws = lws.select("svg");
		sz = lws.style("height");
		if(sz && sz[sz.length - 1] === 'x')  // ...px
			wsHeight = sz.slice(0, -2);
	}
	
	if(naviOffs.x != -1 || naviOffs.y != -1) {
		naviOffs.x += wsWidth - w0;
		naviOffs.y += wsHeight - h0;
	}
	
	if(naviShow)
		d3.select("g#navi").attr("transform", "translate(" + [naviOffs.x, naviOffs.y] + ")");
}

/** Update Text Area input data using default grapth */
function updateUserInput() {
	d3.select(inpElem)
		.call(function(it) {
			var text = "";
			items.forEach(function(e, i) {
				text += i + "; to: " + e.to;
				if(e.from === null)
					text += "; from: null";
				text += "\n";
			});
			it.node().value = text;
		});
}

function generateInpData() {
	// Tidy Error notification
	d3.select(inpErr)
	.call(function tidy(el) {
		el.node().textContent = "";
	});

	// Read Graph generation parameters
	var el = d3.select("#diSize");
	var size = el.node().value;  // 1 .. 1000
	if(!size)
		size = el.attr("placeholder");

	el = d3.select("#diDens");
	var dens = el.node().value;  // 1 .. 100; Max density, mean ~= max / 2
	if(!dens)
		dens = el.attr("placeholder");
	
	if(size < 1 || size > 1000 || dens < 1 || dens > 100) {
		var msg = "Invalid range of graph generation arguments:" + " size = " + size + "; dens = " + dens;
		d3.select(inpErr)
			.call(function tidy(el) {
				el.node().textContent = "ERROR " + msg;
			});
		throw msg;
	}
	
	// Define number of input links for each node
	var inum = [];
	var i;
	for(i = 0; i < size; ++i)
		inum.push(0);
	
	// Generate items
	items = [];
	var to;
	var lnum = 0;
	var lto = 0;
	var z, zMax = size * 2;  // Antihang counter
	// Generate out links only
	for(i = 0; i < size; ++i) {
		to = [];
		lnum = Math.round(Math.random() * dens / 100 * size);
//		console.log("lnum #" + i + ": " + lnum);
		for(var j = 0; j < lnum; ++j) {
			z = 0;
			do {
				lto = Math.round(Math.random() * (size - 1));  // id: 0 .. size-1
//				console.log("lto #" + j + ": " + lto + (to.indexOf(lto) == -1 ? "" : "; index: -1"));
			} while((to.indexOf(lto) != -1 || lto == i) && ++z < zMax);  // Also exclude links to itself
			to.push(lto);
			++inum[lto];
		}
		
		if(to.length)
			to.sort();
		else to = null;
//		console.log("to #" + i + ": " + to);
		items.push(new GraphItem(to));
	}

	// Mark root items
	var from = null;
	lnum = size;
	for(i = 0; i < size; ++i) {
		z = inum[i];
		if(z < lnum)
			lnum = z;
//		console.log("item #" + i + ": " + items[i]);
		if(!z)
			items[i].from = null;
	}
	// In case there are no items without incoming connections then
	// root items are items with the least number of incoming connections
	if(lnum)
		inum.forEach(function defRoot(e, i) {
			if(e == lnum)
				items[i].from = null;
		});
	
	updateUserInput();
	buildViews(items);
}

function initWorkspace() {
	// Initialize workdspace -----------------------------------------------
	var ws = d3.select("#workSpace").append("svg");
	wsResize();
	
	{	// Set adaptive workspace width and height if possible
		var lws = d3.select("#workSpace")  // Attention: otherwise does not work in Firefox: shows 100%
		var sz = lws.style("width");
		if(sz && sz[sz.length - 1] === 'x')  // ...px
			wsWidth = sz.slice(0, -2);
			
		lws = lws.select("svg");
		sz = lws.style("height");
		if(sz && sz[sz.length - 1] === 'x')  // ...px
			wsHeight = sz.slice(0, -2);
	}
	
	//ws.append("title").text("GraViM Workspace")
	ws.append("desc").text("Workspace contains Session View and optional Navigation View in the right bottom corner");

	// Set background
	ws.append("rect")
		.attr("id", "svgbg")
		.attr("width", "100%")
		.attr("height", "100%");

	// Set key listener
	d3.select("body")
		.on("keydown", function() {
			// Rebuild graph on CTRL+Enter or CTRL+b
			if(d3.event.ctrlKey && (d3.event.keyCode == 13 || d3.event.keyCode == 66))
				updateViews();
			else if(d3.event.ctrlKey && d3.event.keyCode == 71) // Auto generate items on CTRL+g, update UI input controls and build views
				generateInpData();
		});

	// Define initial input data
	items = [new GraphItem([2], null),  // Note: null is root hierarchy level indicator
			new GraphItem([2, 3], null),
			new GraphItem([4, 6]),  // , [0, 1]
			new GraphItem([5, 7]),  // , [1]
			new GraphItem([8]),  // , [2]
			new GraphItem([8]),  // , [3]
			new GraphItem([8]),  // , [2]
			new GraphItem(null),  // , [3]
			new GraphItem([9, 10, 11]),  // , [4, 5, 6]
			new GraphItem(null),  // , [8]
			new GraphItem(null),  // , [8]
			new GraphItem(null)];  // , [8])

	updateUserInput();  // Update Text Area input data using default grapth
	
	// Build Views (Navigation & Session)
	buildViews(items);
}

/** Main working function */
(function() {
	window.onload = initWorkspace;  // document.onload is not fired
	window.onresize = wsResize;
})()

// External HTML processing functions ----------------------------------
/** Parses text input
 * elem - HTML element to parce text input from it's node value
 * returns flat array of items - directed graph connectivity matrix
 */
function parseTextInput(elem) {
	var items = [];
	d3.select(elem)
		.call(function(it) {
			var its = it.node().value.split("\n")
			its.forEach(function parceItem(e, j) {
				if(!e.length)
					return;
				var parts = e.split(";");
				var i = 0;
				var to = null;
				var from;
				var v;
				// Skip row number if specified
				if(i < parts.length && !isNaN(v = parseInt(parts[i]))) {
					var msg = "Inconsistent input data: line number does not correspond to the expected value";
					if(v != j) {
						markErrInputLine(j, v, msg);
						console.log("v: " + v + ", j: " + j);
						throw msg;
					}
					++i;
				}
				
				// Parce .to, .from fields
				var t;
				while(i < parts.length) {
					v = parts[i++].split(":");
					t = v[0].trim();
					if(t == "to") {
						if(v.length > 1) {
							to = v[1].split(",");
							for(v = 0; v < to.length; ++v) {
								t = parseInt(to[v]);
								if(isNaN(t)) {
									to = null;
									break;
								}
								to[v] = t;
							}
						}
					}
					else if(t == "from" && v.length > 1)
						if(v[1].trim() != "")
							from = null;
				}
				
				items.push(from === null ? new GraphItem(to, from) : new GraphItem(to));
			});
		});
	return items;
}

function naviResize(el) {
	var r = d3.select(el.parentNode);
	r.selectAll(el.localName)
		.classed("active", false);
	d3.select(el).classed("active", true);
	
	// Update navigation view size
	var szrate = 1;
	if(el.id == "nvszS")
		szrate /= naviSzrateK;
	else if(el.id == "nvszL")
		szrate *= naviSzrateK;
	if(naviSzrate != szrate) {
		naviSzrate = szrate;
		var w0 = naviWidth;
		var h0 = naviHeight;
		naviWidth = 320 * naviSzrate,  // Navigation View width and height
		naviHeight = 240 * naviSzrate,
		naviOffs.x -= naviWidth - w0;
		naviOffs.y -= naviHeight - h0;
		buildNavi(hier, items);
	}
}

function naviUse(el) {
	var de = d3.select(el);
	naviShow = !de.classed("active");
	de.classed("active", naviShow);
	
	if(naviShow)
		d3.select("svg #navi").attr("transform", "translate(" + [naviOffs.x, naviOffs.y] + ")")
	else d3.select("svg #navi").attr("transform", "translate(" + [wsWidth, wsHeight] + ")");
}
